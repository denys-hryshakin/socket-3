import * as config from "./config";
import { texts } from "../data";
import { userJoin, userLeave, getRoomUsers, getCurrentUser } from "./users";

const rooms = [];

const startGame = (roomName) => {
  const users = getRoomUsers(roomName)
  const isReady = users.every(user => user.status === "green");

  return isReady;
}

const getNumberRoomUsers = (io, roomName) => {
  const clients = io.sockets.adapter.rooms[roomName].sockets;
  const numClients = clients ? Object.keys(clients).length : 0;

  return numClients;
}

const addRoom = (roomName, roomNameFailed, isGameStarted) => {
  const room = { roomName, isGameStarted };
  if (rooms.includes(roomName)) {
    roomNameFailed();
    return room;
  }
  rooms.push(room);

  return room;
}

export const getRoom = roomName => {
  return rooms.filter(room => (room.roomName === roomName && room.isGameStarted === false));
}

const getText = () => {
  return texts[0];
}

export default io => {
  io.on("connection", socket => {
    socket.emit("UPDATE_ROOMS", rooms);

    const username = socket.handshake.query.username;
    const userStatus = "red";
    const userProgress = 0;

    socket.on("ADD_ROOM", ({ roomName, isGameStarted }) => {
      const roomNameFailed = () => socket.emit('ROOM_NAME_FAILED');
      addRoom(roomName, roomNameFailed, isGameStarted);
      io.emit("rooms", { rooms: getRoom(roomName) });
    });

    socket.on("JOIN_ROOM", roomName => {
      socket.join(roomName);
      userJoin(socket.id, username, roomName, userStatus, userProgress);
      const userCounter = getNumberRoomUsers(io, roomName)

      // if (numClients <= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      io.to(socket.id).emit("JOIN_ROOM_DONE", ({ userCounter }));
      io.to(roomName).emit("UPDATE_USER_COUNTER", userCounter);
      // }

      io.to(roomName).emit('roomUsers', { users: getRoomUsers(roomName) });
      console.log({ users: getRoomUsers(roomName) })

      socket.on("READY", ({ status }) => {
        const user = getCurrentUser(socket.id);
        user.status = status;
        io.to(roomName).emit("TEXT", ({ text: getText() }))

        io.to(roomName).emit('roomUsers', { users: getRoomUsers(roomName) });
        io.to(roomName).emit("START", { rdyStatus: startGame(roomName) });
        if (startGame(roomName)) {
          var beforeCountdown = config.SECONDS_TIMER_BEFORE_START_GAME;
          var gameCountdown = config.SECONDS_FOR_GAME;
          const timer = setInterval(() => {
            if (beforeCountdown <= 1) {
              clearInterval(timer);
            }
            beforeCountdown -= 1;
            io.to(roomName).emit("timer", { countdown: beforeCountdown });
          }, 1000);

          setTimeout(() => {
            const gameTimer = setInterval(() => {
              if (gameCountdown <= 1) {
                clearInterval(gameTimer);
              }
              gameCountdown -= 1;
              io.to(roomName).emit("gameTimer", { countdown: gameCountdown });
            }, 1000);

          }, 10000);
        }
        socket.on("PROGRESS", progressChange => {
          user.progress = user.progress + progressChange;
          io.to(roomName).emit('roomUsers', { users: getRoomUsers(roomName) });
        })
      });

    });

    socket.on("LEAVE_ROOM", roomName => {
      userLeave(socket.id);
      io.to(roomName).emit('roomUsers', { users: getRoomUsers(roomName) });
      io.to(roomName).emit('rooms', { rooms: getRoom(roomName) });
      let userCounter = getNumberRoomUsers(io, roomName)
      userCounter--;

      io.to(socket.id).emit("LEAVE_ROOM_DONE", ({ userCounter }));
      io.to(roomName).emit("UPDATE_USER_COUNTER", userCounter);
      socket.leave(roomName);
    })

    socket.on("disconnect", roomName => {
      userLeave(socket.id);

      socket.leave(roomName);
    })
  });
};
